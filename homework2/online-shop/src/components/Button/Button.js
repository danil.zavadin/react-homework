import React, {Component} from 'react';

class Button extends Component {

    render() {
        const {text, onClick, className} = this.props
        return (
            <div>
                <button className={className} onClick={onClick}>{text}</button>
            </div>
        );
    }
}

Button.defaultProps = {
    className: ""
}

export default Button;
