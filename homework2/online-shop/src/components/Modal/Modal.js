import React, {Component} from 'react';

class Modal extends Component {

    render() {
        const {headerText, closeButton, text, actions, hide} = this.props
        return (
            <div className="modal-wrapper" onClick={(e)=>{
                    if(e.target === e.currentTarget){
                        hide()
                    }
            }}>
                <div className="modal">
                    <div className="modal__header">
                        {headerText}
                        {closeButton && (<img src="http://localhost:3000/close.svg" alt="close" className="modal__header__cross" onClick={hide}/>)}
                    </div>
                    <p className="modal__body">
                        {text}
                    </p>
                    <div className="modal__buttons-block">
                        {actions.map((button, key) => {
                            if(key === false){      // заглушка
                                console.log(key);   // для
                            }                       // ошибки
                            return (button)
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;
