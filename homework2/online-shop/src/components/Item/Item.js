import PropTypes from 'prop-types';
import React, {Component} from 'react';

class Item extends Component {

    state = {
        isFavorite: null
    }


    componentDidMount() {
        if (localStorage.getItem("favorites")) {
            let favorites = JSON.parse(localStorage.getItem("favorites"))
            const {index} = this.props
            this.setState({
                isFavorite: favorites.find(star => {
                    if (`star${index}` === star) {
                        return true
                    }
                })
            })
        }
    }

    render() {
        const {setFavorite, purchase, productName, productPrice, productPicture, productVendorCode, productColor, index} = this.props
        const {isFavorite} = this.state
        return (
            <div className="item" id={`item${index}`}>

                <div className="item__header">
                    <h6 className="item__header__productName">{productName}</h6>

                    {!isFavorite && <img onClick={(e) => {
                        this.setState({isFavorite: true})
                        setFavorite(e)
                    }} className="item__header__star" src="http://localhost:3000/star.svg" alt="star"
                                         id={`star${index}`}/>}
                    {isFavorite && <img onClick={(e) => {
                        this.setState({isFavorite: false})
                        setFavorite(e)
                    }} className="item__header__star" src="http://localhost:3000/star-filled.svg" alt="star"
                                        id={`star${index}`}/>}
                </div>
                <div className="item__productPicture">
                    <img className="item__productPicture__content" src={productPicture} alt={productName}/>
                </div>

                <div className="item__data">
                    <span className="item__data__color">Color : {productColor}</span>
                </div>

                <div>
                    <div className="item__subpicture-content">
                        <span className="item__subpicture-content__vendor">{productVendorCode}</span>
                        <span className="item__subpicture-content__price">{productPrice}$</span>
                    </div>
                    <button onClick={purchase} className="purchase-button" id={`item${index}`}>Add to Cart</button>
                </div>
            </div>
        );
    }
}

Item.propTypes = {
    productName: PropTypes.string,
    productPicture: PropTypes.string,
    productColor: PropTypes.string,
    productPrice: PropTypes.number,
    productVendorCode: PropTypes.number,
}

Item.defaultProps = {
    productColor: "",
    productPicture: "../../../public/notFound.svg"
}

export default Item;