import React, {Component} from 'react';
import "./App.scss"
import axios from "axios";
import Item from "./components/Item/Item";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

class App extends Component {

    state = {
        currentItem: null,
        itemsList: [],
        modal: false
    }

    componentDidMount() {
        this.getItemsList()
    }

    setFavorite = (e) => {
        if (localStorage.getItem("favorites")) {
            let favorites = JSON.parse(localStorage.getItem("favorites"))
            localStorage.setItem("favorites", JSON.stringify([...favorites, e.target.id]))
            favorites.forEach((i, key) => {
                // console.log(key);
                if (e.target.id === i) {
                    favorites.splice(favorites.indexOf(i), 1)
                    localStorage.setItem("favorites", JSON.stringify([...favorites]))
                }
            })
        } else {
            localStorage.setItem("favorites", JSON.stringify([e.target.id]))
        }
    }

    initiateCurrentItemData = (e) => {
        this.setState({modal: true})
        const {itemsList} = this.state
        this.setState({
            currentItem: itemsList.filter(object => {
                for (const prop in object) {
                    if (e.target.id === `item${object[prop]}`) {
                        return object
                    }
                }
            })[0]
        })
    }

    // ^^ Getting currentItem for modal ^^

    addToCart = () => {
        setTimeout(() => {
            const {currentItem} = this.state
            if (localStorage.getItem("cart")) {
                let cart = JSON.parse(localStorage.getItem("cart"))
                localStorage.setItem("cart", JSON.stringify([...cart, `item${currentItem.index}`]))
                cart.forEach((i, key) => {
                    console.log(key);
                    if (`item${currentItem.index}` === i) {
                        localStorage.setItem("cart", JSON.stringify([...cart]))
                    }
                })
            } else if (!currentItem.index) {
                console.error("Undefined index");
            } else {
                localStorage.setItem("cart", JSON.stringify([`item${currentItem.index}`]))
            }
            this.hideModal()
        }, 50)
    }


    openModal = () => {
        this.setState({modal: true})
    }

    hideModal = () => {
        this.setState({modal: false})
    }

    getItemsList = async () => {
        try {
            let data = await axios.get("http://localhost:3000/items.json").then(res => res.data);
            this.setState({
                itemsList: data.map((item, index) => {
                    const {name, price, vendor, color, picture} = item
                    return {
                        productName: name,
                        productPrice: price,
                        productPicture: picture,
                        productVendorCode: vendor,
                        productColor: color,
                        index: index
                    }
                })
            });
        } catch (err) {
            console.log(err);
        }
    }

    render() {

        const {itemsList, currentItem, modal} = this.state

        return <div className='items-wrapper'>
            {modal && <Modal headerText="Add to cart" closeButton
                             text={`Would you like to add ${currentItem.productName} to your cart?`}
                             actions={[<Button text="yes" onClick={this.addToCart}
                                               className="modal__buttons-block__button modal__buttons-block__button--accept"/>,
                                 <Button text="no" onClick={this.hideModal}
                                         className="modal__buttons-block__button modal__buttons-block__button--cancel "/>]}
                             hide={this.hideModal}/>}


            {itemsList.map((item, index) => {
                    return <Item productName={item.productName} productPrice={item.productPrice}
                                 productPicture={item.productPicture} psroductVendorCode={item.productVendorCode}
                                 productColor={item.productColor} index={index}
                                 purchase={this.initiateCurrentItemData}
                                 setFavorite={this.setFavorite}/>
            })}
        </div>

    }
}

export default App;
