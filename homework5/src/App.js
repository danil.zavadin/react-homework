import React, {useEffect} from 'react';
import "./App.scss"
import {getItemsList, toggleFavorite, getChosenItemData, toggleModal, addItemInStore, deleteItemFromStore, getModalInfo} from "./store/operations"
import {useDispatch, useSelector} from "react-redux"
import AppRoutes from "./routes/AppRoutes"
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";



const App = () => {

    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(getItemsList()) 
    },[dispatch])

    // const itemsList = useSelector(state=>state.products.data)
    
    // const getFavorites = () => {
        
    //     if(localStorage.getItem("favorites")) {

    //         itemsList.forEach(item=>{
    //             if(JSON.parse(localStorage.getItem("favorites")).includes(item.vendor.toString())){
    //                 dispatch(toggleFavorite(item.vendor))
    //             } 
    //         })

    //     } 
    // }

    const setFavoriteInLocalStorage = (e) => {
        dispatch(toggleFavorite(e))
        if (localStorage.getItem("favorites")) {
            let favorites = JSON.parse(localStorage.getItem("favorites"))
            localStorage.setItem("favorites", JSON.stringify([...favorites, e.target.id]))
            favorites.forEach((i, key) => {
                // console.log(key);
                if (e.target.id === i) {
                    favorites.splice(favorites.indexOf(i), 1)
                    localStorage.setItem("favorites", JSON.stringify([...favorites])) 
                }
            })
        } else {
            localStorage.setItem("favorites", JSON.stringify([e.target.id]))
        }
    }

    const vendor = useSelector(state=>state.currentItem)

    const addToCart = () => {
        dispatch(addItemInStore(vendor))
        if (localStorage.getItem("cart")) {
            let cart = JSON.parse(localStorage.getItem("cart"))
            localStorage.setItem("cart", JSON.stringify([...cart, `item${vendor}`]))
        } else if (!vendor) {
            console.error("Undefined product vendor");
        } else {
            localStorage.setItem("cart", JSON.stringify([`item${vendor}`]))
        }
    }
    
    const deleteFromCart = () => {
        dispatch(deleteItemFromStore(vendor))
        if (localStorage.getItem("cart")) {

            let cart = JSON.parse(localStorage.getItem("cart"))

            for(let item of cart){
                if (`item${vendor}` === item) {
                    cart.splice(cart.indexOf(item), 1)
                    localStorage.setItem("cart", JSON.stringify([...cart]))
                    break
                }
            }

        }
    }

    const initiateModalInfo = (e) => {
        if (e.target.getAttribute("data-actiontype") === "delete") {
            dispatch(getModalInfo({
                action: "delete",
                text: `Do you want to delete this from your cart?`
            }))
        } else if (e.target.getAttribute("data-actiontype") === "add") {
            dispatch(getModalInfo({
                action: "add",
                text: `Do you want to add this to your cart?`
            }))
        }
    }

    const { isModal, modalInfo } = useSelector(state=>state)

    // setTimeout(getFavorites(), 3000)

    return (
        <div>
            
            <AppRoutes setFavorite={setFavoriteInLocalStorage} getChosenItemData={getChosenItemData} toggleModal={toggleModal} initiateModalInfo={initiateModalInfo}/>

            {isModal &&
            <Modal headerText={"Confirm action"} closeButton text={modalInfo.text} hide={toggleModal}
                   actions={[
                       <Button text="yes" onClick={()=>{
                           modalInfo.action === "delete" ? deleteFromCart(vendor) : addToCart()
                           dispatch(toggleModal())
                       }}
                               className="modal__buttons-block__button modal__buttons-block__button--accept"/>,
                       <Button text="no" onClick={()=>{dispatch(toggleModal())}}
                               className="modal__buttons-block__button modal__buttons-block__button--cancel "/>]}/>}
        </div>
    )
}

export default App