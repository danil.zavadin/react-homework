import Item from "../../components/Item/Item";
import FormikCart from "../../froms/FormikCart"
import React from "react";

const Cart = ({itemActions, itemsList}) => {

    const cart = itemsList.filter(item => {
        // debugger
        return JSON.parse(localStorage.getItem("cart")) ? JSON.parse(localStorage.getItem("cart")).includes(`item${item.vendor}`) : null
    })

    return (
        
        <div className="items-wrapper__content">

            <div className="items-wrapper__content__products">
                {cart && Array.isArray(cart) && cart.length !== 0 ? cart.map((item, key) => {
                    return <Item key={key} item={item} itemActions={itemActions}/>
                }) : <div className="unloaded-items-warning">No items in cart yet</div>}
            </div>

            <div className="items-wrapper__content__form">
                <FormikCart/>
            </div>

        </div>
        
        

        )
}

export default Cart