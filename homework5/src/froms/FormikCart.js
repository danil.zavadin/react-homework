import React from "react";
import * as Yup from 'yup';
import { Formik, Form } from "formik";
import {useDispatch} from "react-redux"
import {checkoutUserProducts} from "../store/operations"
import CartInput from "./CartInput";

const validationFormSchema = Yup.object().shape({

    name: Yup.string()
        .required('This is necessary field'),
    surname: Yup.string()
        .required('This is necessary field'),
    age: Yup.number()
        .required('This is necessary field'),
    address: Yup.string()
        .required('This is necessary field'),
    phoneNumber: Yup.string()
        .required('This is necessary field')
        .min(13, 'Too Short!')
        .max(13, 'Too Long!')

})



const FormikCart = () => {

  const dispatch = useDispatch()

  const submitForm = (values, props) => {
    const {setSubmitting} = props
    setSubmitting(true)
    
    dispatch(checkoutUserProducts(values))

    setSubmitting(false)
  };

  return (
    <React.Fragment>
      <h1 className="items-wrapper__content__form__header">Checkout your Order</h1>

      <Formik
        initialValues={{
            name: '',
            surname: '',
            age: '',
            address: '',
            phoneNumber: '+380'
        }}
        onSubmit={submitForm}
        validationSchema={validationFormSchema}>

        {(formikProps) => {
          
          return (

            <Form>

            <CartInput name="name" type="text" label="Your Name"/>
            <CartInput name="surname" type="text" label="Your Surname"/>
            <CartInput name="age" type="text" label="Your Age"/>
            <CartInput name="address" type="text" label="Your Address"/>
            <CartInput name="phoneNumber" type="text" label="Your Phone Number"/>
            <button className="items-wrapper__content__form__submit" disabled={formikProps.isSubmitting} type="checkout">Checkout</button>

            </Form>

          );
        }}

      </Formik>
    </React.Fragment>

  );
};

export default FormikCart;
