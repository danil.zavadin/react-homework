import React, {useEffect, useReducer} from 'react';
import {Link, Switch, Route, Redirect} from "react-router-dom";
import "./App.scss"
import axios from "axios";
import Item from "./components/Item/Item";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import Favorites from "./pages/Favorites/Favorites";
import Cart from "./pages/Cart/Cart";


const App = () => {

    const [state, setState] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {itemsList: [],modal: false}
    );

    const {itemsList, modal} = state

    useEffect(() => {
        getItemsList()
    }, [])

    const getItemsList = async () => {
        try {
            let data = await axios.get("/items.json").then(res => res.data)
            setState({
                itemsList: data.map((item) => {
                    const {name, price, vendor, color, picture} = item
                    return {
                        productName: name,
                        productPrice: price,
                        productPicture: picture,
                        productVendorCode: vendor,
                        productColor: color,
                    }
                })
            });
        } catch (err) {
            console.log(err);
        }
    }

    const setFavorite = (e) => {
        if (localStorage.getItem("favorites")) {
            let favorites = JSON.parse(localStorage.getItem("favorites"))
            localStorage.setItem("favorites", JSON.stringify([...favorites, e.target.id]))
            favorites.forEach((i, key) => {
                // console.log(key);
                if (e.target.id === i) {
                    favorites.splice(favorites.indexOf(i), 1)
                    localStorage.setItem("favorites", JSON.stringify([...favorites]))
                }
            })
        } else {
            localStorage.setItem("favorites", JSON.stringify([e.target.id]))
        }
    }

// ^^ Getting currentItem for modal ^^

    const addToCart = () => {
        const {currentItem} = state

        if (localStorage.getItem("cart")) {
            let cart = JSON.parse(localStorage.getItem("cart"))
            localStorage.setItem("cart", JSON.stringify([...cart, `item${currentItem.productVendorCode}`]))
            cart.forEach((i, key) => {
                if (`item${currentItem.productVendorCode}` === i) {
                    localStorage.setItem("cart", JSON.stringify([...cart]))
                }
            })

        } else if (!currentItem.productVendorCode) {
            console.error("Undefined productVendorCode");
        } else {
            localStorage.setItem("cart", JSON.stringify([`item${currentItem.productVendorCode}`]))
        }
        hideModal()
    }

    const deleteFromCart = () => {

        const {currentItem} = state

        if (localStorage.getItem("cart")) {
            let cart = JSON.parse(localStorage.getItem("cart"))
            cart.forEach((i, key) => {
                // console.log(key);
                if (`item${currentItem.productVendorCode}` === i) {
                    cart.splice(cart.indexOf(i), 1)
                    localStorage.setItem("cart", JSON.stringify([...cart]))
                }
            })
        } else {
            localStorage.setItem("cart", JSON.stringify([`item${currentItem.productVendorCode}`]))
        }
    }

    const initiateModalInfo = (e) => {

        if (e.target.getAttribute("data-actiontype") === "delete") {
             setState({
                 modalInfo: {
                    action: "delete",
                    text: `Do you want to delete  from your cart?`
                }
            })
        } else {
             setState({
                modalInfo: {
                    action: "add",
                    text: `Do you want to add  to your cart?`
                }
            })
        }
    }

    const getChosenItemData = async (e) => {
        const {itemsList} = state
        setState({
            currentItem: await itemsList.find(item => {
                return `item${item.productVendorCode}` === e.target.id
            })
        })
    }

    const hideModal = () => {
        setState({modal: false})
    }

    const showModal = () => {
        setTimeout(()=>{setState({modal: true})}, 200)
    }


    return (
        <div className='items-wrapper'>
            <nav className="items-wrapper__navbar">
                <ul className="items-wrapper__navbar__list">
                    <li>
                        <Link to="/" className="items-wrapper__navbar__list__item">Home</Link>
                    </li>
                    <li>
                        <Link to="/favorite" className="items-wrapper__navbar__list__item">Favorite</Link>
                    </li>
                    <li>
                        <Link to="/cart" className="items-wrapper__navbar__list__item">Cart</Link>
                    </li>
                </ul>
            </nav>

            <Switch>
                <Route exact path="/">
                    <Redirect from="/" to="/home"/>
                </Route>
                <Route path="/favorite"
                       render={() => <Favorites itemsList={itemsList} addToCart={addToCart} setFavorite={setFavorite}
                                                getChosenItemData={getChosenItemData} initiateModalInfo={initiateModalInfo} showModal={showModal}/>}>
                </Route>
                <Route path="/cart"
                       render={() => <Cart itemsList={itemsList} addToCart={addToCart} setFavorite={setFavorite}
                                           getChosenItemData={getChosenItemData} initiateModalInfo={initiateModalInfo} showModal={showModal}/>}>
                </Route>
                <Route path="/home">
                    {itemsList.map((item) => {
                        return <Item productName={item.productName} productPrice={item.productPrice}
                                     productPicture={item.productPicture} productVendorCode={item.productVendorCode}
                                     productColor={item.productColor} setFavorite={setFavorite}
                                     getChosenItemData={getChosenItemData} showModal={showModal} initiateModalInfo={initiateModalInfo}
                        />
                    })}
                </Route>
            </Switch>

            {modal &&
            <Modal headerText={"Confirm action"} closeButton text={state.modalInfo.text} hide={hideModal}
                   actions={[
                       <Button text="yes" onClick={()=>{
                           state.modalInfo.action === "delete" ? deleteFromCart() : addToCart()
                           hideModal()
                       }}
                               className="modal__buttons-block__button modal__buttons-block__button--accept"/>,
                       <Button text="no" onClick={hideModal}
                               className="modal__buttons-block__button modal__buttons-block__button--cancel "/>]}/>}
        </div>
    )
}

export default App