import Item from "../../components/Item/Item";
import React from "react";

const Favorites = ({itemsList ,initiateModalInfo, setFavorite, getChosenItemData, showModal}) => {
    const favorites = itemsList.filter(item => {
        return  JSON.parse(localStorage.getItem("favorites")).includes(`star${item.productVendorCode}`)
    })

    return Array.isArray(favorites) && favorites.length !== 0 ? favorites.map((item) => {
        return <Item productName={item.productName} productPrice={item.productPrice}
                     productPicture={item.productPicture} productVendorCode={item.productVendorCode}
                     productColor={item.productColor} setFavorite={setFavorite}
                     getChosenItemData={getChosenItemData} showModal={showModal} initiateModalInfo={initiateModalInfo} />}) : <div className="unloaded-items-warning">No items in favorites yet</div>
}

export default Favorites