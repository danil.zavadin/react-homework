import Item from "../../components/Item/Item";
import React from "react";

const Cart = ({itemsList, initiateModalInfo, setFavorite, getChosenItemData, showModal}) => {
    const cart = itemsList.filter(item => {
        return  JSON.parse(localStorage.getItem("cart")).includes(`item${item.productVendorCode}`)
    })

    return Array.isArray(cart) && cart.length !== 0 ? cart.map((item) => {
        return <Item productName={item.productName} productPrice={item.productPrice}
                     productPicture={item.productPicture} productVendorCode={item.productVendorCode}
                     productColor={item.productColor} setFavorite={setFavorite}
                     getChosenItemData={getChosenItemData} showModal={showModal} initiateModalInfo={initiateModalInfo} />}) : <div className="unloaded-items-warning">No items in cart yet</div>
}

export default Cart