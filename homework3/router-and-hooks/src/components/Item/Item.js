import PropTypes from 'prop-types';
import React, {useReducer, useEffect} from 'react';

const Item = ({setFavorite,initiateModalInfo,getChosenItemData,showModal, productName, productPrice, productPicture, productVendorCode, productColor}) => {

    const [state, setState] = useReducer(
        (state, newState) => ({...state, ...newState}),
        {isFavorite: null}
    )

    useEffect(() => {
        if (localStorage.getItem("favorites")) {
            let favorites = JSON.parse(localStorage.getItem("favorites"))
            setState({
                isFavorite: favorites.find(star => {
                    if (`star${productVendorCode}` === star) {
                        return true
                    }
                })
            })
        }
    }, [productVendorCode])

    Item.propTypes = {
        productName: PropTypes.string.isRequired,
        productPicture: PropTypes.string,
        productColor: PropTypes.string,
        productPrice: PropTypes.number.isRequired,
        productVendorCode: PropTypes.number.isRequired,
    }

    Item.defaultProps = {
        productColor: "",
        productPicture: "../../../public/notFound.svg"
    }


    return (
        <div className="item" id={`item${productVendorCode}`}>

            <div className="item__header">
                <h6 className="item__header__productName">{productName}</h6>

                <div className="item__header__icons">

                    {!state.isFavorite && <img onClick={(e) => {
                        setState({isFavorite: true})
                        setFavorite(e)
                    }} className="item__header__icons__star" src="http://localhost:3000/star.svg" alt="star" id={`star${productVendorCode}`}/>}
                    {state.isFavorite && <img onClick={(e) => {
                        setState({isFavorite: false})
                        setFavorite(e)
                    }} className="item__header__icons__star" src="http://localhost:3000/star-filled.svg" alt="star" id={`star${productVendorCode}`}/>}

                    <img onClick={(e)=> {
                        getChosenItemData(e)
                        initiateModalInfo(e)
                        showModal()
                    }} className="item__header__icons__delete" src="http://localhost:3000/delete.svg" alt="delete" data-actiontype="delete" id={`item${productVendorCode}`}/>

                </div>
            </div>
            <div className="item__productPicture">
                <img className="item__productPicture__content" src={productPicture} alt={productName}/>
            </div>

            <div className="item__data">
                <span className="item__data__color">Color : {productColor}</span>
            </div>

            <div>
                <div className="item__subpicture-content">
                    <span className="item__subpicture-content__vendor">{productVendorCode}</span>
                    <span className="item__subpicture-content__price">{productPrice}$</span>
                </div>
                <button onClick={(e)=>{
                    getChosenItemData(e)
                    initiateModalInfo(e)
                    showModal()
                }} className="purchase-button" data-actiontype="add" id={`item${productVendorCode}`}>Add to Cart</button>
            </div>
        </div>
    );
}

export default Item;