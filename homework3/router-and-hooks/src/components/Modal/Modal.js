import React from 'react';

const Modal = (props) => {
    const {headerText, closeButton, text, actions, hide} = props
    return (
        <div className="modal-wrapper" onClick={(e) => {
            if (e.target === e.currentTarget) {
                hide()
            }
        }}>
            <div className="modal">
                <div className="modal__header">
                    {headerText}
                    {closeButton && (
                        <img src="http://localhost:3000/close.svg" alt="close" className="modal__header__cross"
                             onClick={hide}/>)}
                </div>
                <p className="modal__body">
                    {text}
                </p>
                <div className="modal__buttons-block">
                    {actions.map((button, key) => {
                        return (button)
                    })}
                </div>
            </div>
        </div>
    );
}

export default Modal;
