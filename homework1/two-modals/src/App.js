import Button from '../src/components/Button/Button';
import Modal from './components/Modal/Modal.js';
import './App.scss';
import React, {Component} from 'react';


export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: null
        }
    }

    showFirstModal = () => {
        this.setState({
            status: "first-modal"
        })
    }

    showSecondModal = () => {
        this.setState({
            status: "second-modal"
        })
    }

    hideModal = () => {
        this.setState({
            status: null
        })
    }

    render() {
        return (
            <div>
                <Button backgroundColor="#ff6961" text="Open first modal" onClick={this.showFirstModal}/>
                <Button backgroundColor="#add8e6" text="Open second modal" onClick={this.showSecondModal}/>


                {this.state.status === "first-modal" &&
                <Modal headerText={"Do you want to delete this file?"} closeButton={true}
                       text={"Once you delete this file, it won’t be possible to undo this action"} actions={[
                    <Button backgroundColor="red" text="Ok" onClick={this.test} className="modal__buttons"/>,
                    <Button backgroundColor="red" text="Cancel" onClick={this.test} className="modal__buttons"/>
                ]} status={this.hideModal}/>}


                {this.state.status === "second-modal" &&
                <Modal headerText={"Pepsi is better that Cola"} closeButton={false}
                       text={"Do you agree?"} actions={[
                    <Button backgroundColor="blue" text="Yes" onClick={this.test} className="modal__buttons"/>,
                    <Button backgroundColor="blue" text="No" onClick={this.test} className="modal__buttons"/>
                ]} status={this.hideModal}/>}

            </div>)
    }
}
