import { useField } from "formik";
import React from "react";
import './cartInput.scss'

const CartInput = (props) => {

    const {type, label, name} = props
    const [field, meta] = useField(name);

    return (

        <div>
            <div>
                <label className={'input'}>
                    <h5 className={'input__title'}>{label}</h5>
                    <input className={'input__field'} type={type}  {...field} />
                </label>
            </div>
            {meta.error && meta.touched && (
                <span className="error">{meta.error}</span>
            )}
        </div>

    );
};

export default CartInput;