import React from 'react';

const Button = ({text="button", onClick, className}) => {

    Button.defaultProps = {
        className: ""
    }

    return (
        <div>
            <button data-testid="button-test" className={className} onClick={onClick}>{text}</button>
        </div>
    );

}

export default Button;
