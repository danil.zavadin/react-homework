import React from 'react';
import {useDispatch} from 'react-redux';

const Modal = (props) => {

    const dispatch = useDispatch()

    const {headerText, closeButton, text, actions, hide} = props
    return (
        <div className="modal-wrapper" onClick={(e) => {
            if (e.target === e.currentTarget) {
                dispatch(hide())
            }
        }}>
            <div className="modal">
                <div className="modal__header">
                    <h6 data-testid="modal-heading">{headerText}</h6>
                    {closeButton && (
                        <img data-testid="modal-cross" src="http://localhost:3000/close.svg" alt="close" className="modal__header__cross"
                             onClick={()=>{
                                dispatch(hide())
                             }}/>)}
                </div>
                <p data-testid="modal-text" className="modal__body">
                    {text}
                </p>
                <div className="modal__buttons-block">
                    {actions ? actions.map(button => {
                        return (button)
                    }) : <button onClick={
                                ()=>{dispatch(hide())}
                            }>
                            close
                        </button>}
                </div>
            </div>
        </div>
    );
}

export default Modal;
