import Modal from "./Modal"
import Button from "../Button/Button"
import { render } from "@testing-library/react"
import { Provider } from "react-redux"
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

describe('testing Modal', () => {
    
    const initialState = {}
    const store = mockStore(initialState)

    test("modal smoke test",()=>{
        render(<Provider store={store}><Modal/></Provider>)
    })

    const actionsMock = [<Button text="test1" onClick={jest.fn()}/>, <Button text="hello now" onClick={jest.fn()}></Button>]

    test("should render modal with props",()=>{
        const { getByTestId } = render(<Provider store={store}><Modal headerText="modal" closeButton text="modal text" actions={actionsMock} hide={jest.fn()}/></Provider>)
        const modalHeading = (getByTestId("modal-heading")).textContent
        const modalText = (getByTestId("modal-text")).textContent
        expect(modalHeading).toBe("modal")
        expect(modalText).toBe("modal text")
        expect(getByTestId("modal-cross")).toBeInTheDocument()
        // debug()
    })

    test("should render specific buttons",()=>{
        const { getByText } = render(<Provider store={store}><Modal actions={actionsMock} /></Provider>)
        expect(getByText("test1")).toBeInTheDocument()
        expect(getByText("hello now")).toBeInTheDocument()
        // debug()
    })
})