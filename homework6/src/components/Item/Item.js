
import {useDispatch} from 'react-redux'
import React from 'react';

const Item = ({itemActions, item}) => {

    const {setFavorite, getChosenItemData, toggleModal, initiateModalInfo, guaranteedIsFavorite} = itemActions 
    const {name, picture, color, price, vendor, isFavorite, inCart} = item
    const dispatch = useDispatch()

    return (
        <div className="item" id={`item${vendor}`}>

            <div className="item__header">
                <h6 className="item__header__productName">{name}</h6>

                <div className="item__header__icons">

                    {!isFavorite && <img onClick={(e) => {setFavorite(e)}} className="item__header__icons__star" src="/star.svg" alt="star" id={`${vendor}`}/>}
                    {(isFavorite || guaranteedIsFavorite) && <img onClick={(e) => {setFavorite(e)}} className="item__header__icons__star" src="/star-filled.svg" alt="star" id={`${vendor}`}/>}

                    {inCart > 0 && <img onClick={(e)=> {
                        dispatch(getChosenItemData(e))
                        initiateModalInfo(e)
                        dispatch(toggleModal())
                    }} className="item__header__icons__delete" src="/delete.svg" alt="delete" data-actiontype="delete" id={`item${vendor}`}/>}

                </div>
            </div>
            <div className="item__productPicture">
                <img className="item__productPicture__content" src={picture} alt={name}/>
            </div>

            <div className="item__data">
                <span className="item__data__color">Color : {color}</span>
            </div>

            <div>
                <div className="item__subpicture-content">
                    <span className="item__subpicture-content__vendor">{vendor}</span>
                    <span className="item__subpicture-content__price">{price}$</span>
                </div>
                <button onClick={(e)=>{
                    dispatch(getChosenItemData(e))
                    initiateModalInfo(e)
                    dispatch(toggleModal())
                }} className="purchase-button" data-actiontype="add" id={`item${vendor}`}>Add to Cart | {inCart}</button>
            </div>
        </div>
    );
}

export default Item;