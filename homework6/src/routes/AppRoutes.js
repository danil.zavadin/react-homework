import React from 'react'
import {NavLink, Switch, Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux"
import Item from "../components/Item/Item";
import Favorites from "../pages/Favorites/Favorites";
import Cart from "../pages/Cart/Cart";

const AppRoutes = ({setFavorite, getChosenItemData, toggleModal, initiateModalInfo}) => {

    const itemActions = {
        setFavorite: setFavorite,
        getChosenItemData: getChosenItemData,   
        toggleModal: toggleModal,
        initiateModalInfo: initiateModalInfo
    }

    const itemsList = useSelector(state=>state.products.data)

    return (
        <div className='items-wrapper'>
            <nav className="items-wrapper__navbar">
                <ul className="items-wrapper__navbar__list">
                    <li>
                        <NavLink to="/" className="items-wrapper__navbar__list__item">Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/favorite" className="items-wrapper__navbar__list__item">Favorite</NavLink>
                    </li>
                    <li>
                        <NavLink to="/cart" className="items-wrapper__navbar__list__item">Cart</NavLink>
                    </li>
                </ul>
            </nav>

            <Switch>
                <Route exact path="/">
                    <Redirect from="/" to="/home"/>
                </Route>
                <Route path="/favorite" render={() => <Favorites itemsList={itemsList} itemActions={itemActions}/>}/>
                <Route path="/cart" render={() => <Cart itemsList={itemsList} itemActions={itemActions}/>}/>
                <Route path="/home">
                    {itemsList.map((item, key) => {
                        return <Item key={key} item={item} itemActions={itemActions}/>
                    })}
                </Route>
            </Switch>
        </div>
    )
}

export default AppRoutes
