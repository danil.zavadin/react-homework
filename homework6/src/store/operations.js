import {GET_ITEMS_DATA, TOGGLE_MODAL, TOGGLE_FAVORITE, GET_CHOSEN_ITEM_DATA, ADD_CART_ITEM, DELETE_CART_ITEM, GET_MODAL_INFO, CHECKOUT_USER_DATA} from "./types"
import axios from "axios"
// {GET_ITEMS_DATA, TOGGLE_MODAL, TOGGLE_FAVORITE, GET_CHOSEN_ITEM_DATA, ADD_CART_ITEM, DELETE_CART_ITEM, GET_MODAL_INFO, CHECKOUT_USER_DATA}
export const getItemsList = () => (dispatch) => {
    axios("http://localhost:3000/items.json").then(res=> dispatch({type:GET_ITEMS_DATA, payload: res.data}))
}

export const toggleFavorite = (e) => (dispatch) => {
    dispatch({type: TOGGLE_FAVORITE, payload: e.target !== undefined ? e.target.id : e})
}

export const toggleModal = () => (dispatch) => {
    dispatch({type: TOGGLE_MODAL, payload: null})
}

export const getModalInfo = (data) => (dispatch) => {
    dispatch({type: GET_MODAL_INFO, payload: data})
}

export const getChosenItemData = (e) => (dispatch) => {
    dispatch({type: GET_CHOSEN_ITEM_DATA, payload: e.target.id})
}

export const addItemInStore = (data) => (dispatch) => {
    dispatch({type: ADD_CART_ITEM, payload: data})
}

export const deleteItemFromStore = (data) => (dispatch) => {
    dispatch({type: DELETE_CART_ITEM, payload: data})
}

export const checkoutUserProducts = (data) => (dispatch) => {
    dispatch({type: CHECKOUT_USER_DATA, payload: data})
}


