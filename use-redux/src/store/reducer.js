
import {GET_ITEMS_DATA, TOGGLE_MODAL, TOGGLE_FAVORITE, GET_CHOSEN_ITEM_DATA, ADD_CART_ITEM, DELETE_CART_ITEM, GET_MODAL_INFO} from './types'

const initialState = {
    products: {
        data: [],
        isLoading: true
    },
    currentItem: {},
    modalInfo: {},
    isModal: false
}

const reducer = (state = initialState, action) => {
    switch(action.type){

        case GET_ITEMS_DATA : 
        let items = action.payload.map((item) => {
            item.isFavorite = false
            item.inCart = 0
            return item})
        return {...state, products: {isLoading: false, data: items}}

        case TOGGLE_FAVORITE :
            let dataUpdatedFavorites = state.products.data.map(item => +action.payload === item.vendor ? {...item, isFavorite : !item.isFavorite } : item)
            return {...state, products: {...state.products, data : dataUpdatedFavorites }}

        case ADD_CART_ITEM :
            let dataRenewedCart = state.products.data.map(item => action.payload === item.vendor ? {...item, inCart : item.inCart + 1} : item)
            return {...state, products: {isLoading: false, data : dataRenewedCart }}

        case DELETE_CART_ITEM :
            let dataDeprecatedCart = state.products.data.map(item => action.payload === item.vendor && item.inCart !== 0 ? {...item, inCart : item.inCart - 1} : item)
            return {...state, products: {isLoading: false, data : dataDeprecatedCart }}    

        case TOGGLE_MODAL :
            return {...state, isModal: !state.isModal}
        
        case GET_CHOSEN_ITEM_DATA :
            let currentItem = state.products.data.find(item =>  {
                return action.payload === `item${item.vendor}`}) 
            return {...state, currentItem: currentItem.vendor}

        case GET_MODAL_INFO:
            return {...state, modalInfo: action.payload}

        default: return state
    }
}

export default reducer