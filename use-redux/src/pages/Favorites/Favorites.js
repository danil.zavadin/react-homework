import Item from "../../components/Item/Item";
import React from "react";

const Favorites = ({itemActions, itemsList}) => {

    const favorites = itemsList.filter(item => {
        // debugger
        return JSON.parse(localStorage.getItem("favorites")) ? JSON.parse(localStorage.getItem("favorites")).includes(`${item.vendor}`) : null
    })

    return (
            Array.isArray(favorites) && favorites.length !== 0 ? favorites.map((item, key) => {
                return <Item key={key} item={item} itemActions={itemActions} guaranteedIsFavorite={true}/>
            }) 
            : <div className="unloaded-items-warning">No items in favorites yet</div>)
    }
export default Favorites