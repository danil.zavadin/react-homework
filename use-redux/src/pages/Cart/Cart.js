import Item from "../../components/Item/Item";
import React from "react";

const Cart = ({itemActions, itemsList}) => {

    const cart = itemsList.filter(item => {
        // debugger
        return JSON.parse(localStorage.getItem("cart")) ? JSON.parse(localStorage.getItem("cart")).includes(`item${item.vendor}`) : null
    })

    return cart && Array.isArray(cart) && cart.length !== 0 ? cart.map((item, key) => {
        return <Item key={key} item={item} itemActions={itemActions}/>
    }) : <div className="unloaded-items-warning">No items in cart yet</div>
}

export default Cart